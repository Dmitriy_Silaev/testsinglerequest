﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
namespace Answer485
{
    public partial class Form1 : Form
    {
        public static Mutex mtxcom1 = new Mutex();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            // передаем в конструктор тип класса
            XmlSerializer formatter1 = new XmlSerializer(typeof(Com));
           
            // десериализация
            using (FileStream fs1 = new FileStream("comone.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    Com newPerson1 = (Com)formatter1.Deserialize(fs1);

                    //Console.WriteLine("Имя: {0} --- Возраст: {1}", newPerson.Name, newPerson.Baudrate);
                    if (!serialPort1.IsOpen)
                    {
                        serialPort1.PortName = newPerson1.Name;
                        serialPort1.BaudRate = newPerson1.Baudrate;//comboBox1.SelectedItem.ToString();
                        serialPort1.Open();
                        //Com1PortExist = 1;
                        label1.Text = serialPort1.PortName;
                        label2.Text = serialPort1.BaudRate.ToString();
                        comboBox1.Text = serialPort1.BaudRate.ToString();
                    }
                }
                catch (InvalidOperationException)
                {
                    //MessageBox.Show("Настройте конфигурацию");
                    //Close();
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (UnauthorizedAccessException)
                {
                    //MessageBox.Show("Отказ в доступе к порту load");
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (ArgumentNullException)
                {
                    ///пустота нет такого значения
                    groupBox1.BackColor = Color.LightPink;
                }
                catch (IOException)
                {
                    //MessageBox.Show("Отсутствует ком порт");
                    groupBox1.BackColor = Color.LightPink;
                    //Close();
                }
            }

         

            try
            {
                string[] str = System.IO.Ports.SerialPort.GetPortNames();
                //this.listBox1.Items.AddRange(new object[] {"456"});
                listBox1.Items.AddRange(str);
                listBox1.SetSelected(0, true);
                listBox1.SelectedItem = serialPort1.PortName;
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("Подключите устройство к порту (нет ком порта)");
                Close();
            }



          
                var pollingThread = new Thread(Polling1);
                pollingThread.IsBackground = true;
                pollingThread.Start();
          

            //////////////////////////////////////////////////////////////////////////        
        }
        private void Polling1()
        {
            long b = 0;
            int sent = 0;
            int received = 0;

            //byte[] InBuffer = new byte[1024];
           Thread.Sleep(3000);
            while (true)
            {
                if (serialPort1.IsOpen)
                {
                   
                        byte[] MidBuffer = new byte[1024];
                        byte[] InBuffer = new byte[1024];
                        Random rnd = new Random();
                        Random rndln = new Random();

                        int obj = rnd.Next(1, 10);
                        int objln = rndln.Next(100, 100);

                        for (int i = 0; i < objln; i++)
                            InBuffer[i] = (byte)obj;

                        int Count = objln;
/*
                        int u = 0;
                      
                        byte GlobalHostNum = 1;
                        MidBuffer[0] = 0x80;
                        MidBuffer[1] = 0x80;
                        MidBuffer[2] = 0x80;
                        MidBuffer[3] = 0x01;
                        MidBuffer[4] = 0x00;//длинна пакета
                        MidBuffer[5] = 0x00;//длинна пакета
                        MidBuffer[6] = GlobalHostNum;//адрес отправителя
                        MidBuffer[7] = 0x01;//адрес получателя
                        MidBuffer[8] = 0x00;//номер 
                        MidBuffer[9] = 0x00;//пакета
                        MidBuffer[10] = 0x03;//ф-я запись
                        MidBuffer[11] = 0x01;//кол-во индекс команд
                        MidBuffer[12] = 0x20;//индексированная
                        MidBuffer[13] = 0x03;//команда
                        MidBuffer[14] = 0x01;//номер
                        MidBuffer[15] = 0x00;//структуры
                        MidBuffer[16] = (byte)(Count);//длинна
                        MidBuffer[17] = (byte)(Count >> 8);   //данных
                        for (u = 0; u < Count; u++)
                        {
                            MidBuffer[18 + u] = InBuffer[u];
                        }

                        MidBuffer[4] = (byte)((18 + u + 1 + 1));//длинна пакета
                        MidBuffer[5] = (byte)((18 + u + 1 + 1) >> 8);//длинна пакета

                        int my_crc16 = CRC_16(MidBuffer, (18 + u), 0);
                        MidBuffer[18 + u] = (byte)my_crc16;
                        MidBuffer[18 + u + 1] = (byte)(my_crc16 >> 8);

 */                      
                        //serialPort1.Write(MidBuffer, 0, (18 + u + 1 + 1));
                        Stopwatch SW = new Stopwatch(); // Создаем объект
                        SW.Start();
                        serialPort1.Write(InBuffer, 0, objln); 
                        //serialPort1.Write(MidBuffer, 0, (18 + u + 1 + 1));
                        sent++;
                        label5.Invoke(new Action<String>(t => label5.Text = t), Convert.ToString(sent));
                      
                        Count = 0;
                        try
                        {
                            serialPort1.ReadTimeout = -1;
                            InBuffer[0] = (byte)serialPort1.ReadByte();
                        }
                        catch (Exception ex)
                        {
                            serialPort1.ReadTimeout = -1;
                            Thread.Sleep(500);
                            continue;
                        }
                        Count++;
                        mtxcom1.WaitOne();
                        int u = 1;
                        //serialPort1.ReadTimeout = 200;
                        try
                        {
                            //for (u = 1; u < 1024; u++)
                            //{
                               // InBuffer[u] = (byte)serialPort1.ReadByte();
                            //}
                            
                            while (serialPort1.BytesToRead != 0)
                            {

                                byte[] data = new byte[serialPort1.BytesToRead];
                                serialPort1.Read(data, 0, data.Length);
                                Array.Copy(data, 0, InBuffer, 1, data.Length);
                                Count += data.Length;
                            }
                            
                        }
                        catch (Exception ex)
                        {
                            //MessageBox.Show("Не удалось получить ответ"); 
                            serialPort1.ReadTimeout = -1;
                            //mtxcom1.ReleaseMutex();
                            //Thread.Sleep(500);
                            //continue;
                        }
                        mtxcom1.ReleaseMutex();

                        SW.Stop(); //Останавливаем
                        received++;
                        label6.Invoke(new Action<String>(t => label6.Text = t), Convert.ToString(received));
                        //label3.Text = Convert.ToString(SW.ElapsedMilliseconds);
                        label3.Invoke(new Action<String>(t => label3.Text = t), Convert.ToString(SW.ElapsedMilliseconds));

                        long a = SW.ElapsedMilliseconds;
                        if (a > b)
                        {
                            b = a;
                            label4.Invoke(new Action<String>(t => label4.Text = t), Convert.ToString(b));
                        }
                    if(a > 250)
                    {
                        StreamWriter sw;
                        sw = File.AppendText("logfile.txt");
                        sw.WriteLine("больше 250" + "  " + Convert.ToString(a) + " " + DateTime.Now);
                        sw.Close();
                    }

                    if (objln != Count)
                    {
                        StreamWriter sw;
                        sw = File.AppendText("comparedln.txt");
                        sw.WriteLine("не совпала длинна" + "  " + Convert.ToString(objln) + "  " + Convert.ToString(Count) + " " + DateTime.Now);
                        sw.Close();
                        Thread.Sleep(500);
                        continue;
                    }

                    for (int i = 0; i < objln;i++ )
                    {
                        if(InBuffer[i]!= obj)
                        {
                            StreamWriter sw;
                            sw = File.AppendText("compareddata.txt");
                            sw.WriteLine("не совпали данные" + "  " + Convert.ToString(InBuffer[i]) + "  " + Convert.ToString(obj) + " " + DateTime.Now);
                            sw.Close();
                            break;
                        }
                    }

                        Thread.Sleep(500);
                      
                       
                  
                   
              
                }
                else
                {
                    Thread.Sleep(50);
                }
            }
        }

        static int CRC_16(byte[] bytes, int len, byte flag)//byte[] bytes//int CRC_16(byte* buffer, int len, byte flag)
        {
            int crc = 0xffff;
            int ind = 0;
            while (len > 0)
            {
                crc = crc16_update(crc, bytes[ind]);//crc=crc16_update(crc,*buffer)
                ind++;//buffer++
                len--;
            }
            if (flag > 0)
            {
                crc = crc & 0x0000;
                crc++;
            }
            return crc;
        }

        static int crc16_update(int crc, byte a)
        {
            int i;

            crc ^= a;
            for (i = 0; i < 8; ++i)
            {
                if ((crc & 1) != 0)
                    crc = (crc >> 1) ^ 0xA001;
                else
                    crc = (crc >> 1);
            }

            return crc;
        }
        private void button1_Click(object sender, EventArgs e)//открыть порт
        {

            try
            {
                

                mtxcom1.WaitOne();
                if (serialPort1.IsOpen) serialPort1.Close();
                ///mtxcom1.ReleaseMutex();

                //mtxcom1.ReleaseMutex();
                //mtxcom1.WaitOne();
                serialPort1.PortName = listBox1.SelectedItem.ToString();
                serialPort1.BaudRate = int.Parse(comboBox1.SelectedItem.ToString());//
                mtxcom1.ReleaseMutex();
                mtxcom1.WaitOne();

                serialPort1.Open();
                label1.Text = serialPort1.PortName;
                label2.Text = serialPort1.BaudRate.ToString();

                groupBox1.BackColor = System.Drawing.SystemColors.Control;
                // объект для сериализации
                Com com = new Com(serialPort1.PortName, serialPort1.BaudRate);
               

                // передаем в конструктор тип класса
                XmlSerializer formatter = new XmlSerializer(typeof(Com));

                // получаем поток, куда будем записывать сериализованный объект
                using (FileStream fs = new FileStream("comone.xml", FileMode.Truncate))
                {
                    formatter.Serialize(fs, com);
                }

                mtxcom1.ReleaseMutex();


            }
            catch (Exception ex)//
            {
                groupBox1.BackColor = Color.LightPink;
                mtxcom1.ReleaseMutex();
            }



         


           

        }
        [Serializable]

        public class Com
        {

            public string Name { get; set; }
            public int Baudrate { get; set; }




            // стандартный конструктор без параметров
            public Com()
            { }

            public Com(string name, int baudrate)
            {

                Name = name;
                Baudrate = baudrate;

            }
        }

        public class Ip
        {

            public string IpNum { get; set; }
            public int Port { get; set; }
            public int HostNum { get; set; }




            // стандартный конструктор без параметров
            public Ip()
            { }

            public Ip(string ipnum, int port, int hostnum)
            {

                IpNum = ipnum;
                Port = port;
                HostNum = hostnum;

            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
